<!DOCTYPE HTML>
<!--
  2019 May 26.
  Ryuichi Hashimoto.
 -->
<html lang="ja">
<head>
<meta charset="utf-8">
<title>捨てるもの一覧</title>
</head>
<body>
<p><?php print("捨てちゃうよ"); ?></p>
<p>
<?php
$directory = "dispose-list";
chdir ($directory);
$path = getcwd();
/*
echo $path;
echo "<BR>";
*/

$count = 1;
foreach (glob("*.jpg") as $filename) {
  /*
  echo $filename; 
  echo "<BR>";
   */

  $fNameWithoutExt = rtrim(${filename}, ".jpg");
  /*
  echo "${fNameWithoutExt}<br>";
   */

  echo "${count}　";
  $fp = fopen("${fNameWithoutExt}-location.txt", "r");
  while (${line} = fgets($fp)) {
    echo "${line}　　　";
  }
  fclose($fp);

  $fp = fopen("${fNameWithoutExt}-comment.txt", "r");
  while (${line} = fgets($fp)) {
    echo "${line}<br>";
  }
  fclose($fp);
 
  echo "<img src=\"${directory}/${filename}\"><BR><BR><BR>";

  $count += 1;
}
?>
<p>
</body>
</html>

